import { Request, Response, NextFunction } from 'express'
interface JSONResponse {
	statusCode: number
	message: string
	data?: Data
	error?: unknown
}

type TypedResponse<T> = Omit<Response, 'json' | 'status'> & {
	json(data: T): TypedResponse<T>
} & { status(code: number): TypedResponse<T> }

declare global {
	type Data = Record<string, unknown> | Array<unknown> | unknown
	type AppResponse = TypedResponse<JSONResponse>
	type Controller = (request: Request, response: AppResponse, next: NextFunction) => Promise<AppResponse>
}
