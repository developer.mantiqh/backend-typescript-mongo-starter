import { Router } from 'express'

import Auth from './auth'

const router = Router()

router.get('/', (_, response) =>
	response.status(200).json({ statusCode: 200, message: 'API working fine.', data: null })
)

router.use('/auth', Auth)

export default router
