import { connect as connectDB } from 'mongoose'

import { Logger } from '@utils/logger'

class DBManager {
	static async connect() {
		try {
			await connectDB(process.env.DB_URL as string)
			Logger.info('Database connected.')
		} catch (error) {
			Logger.error('Error connecting DB.', error)
			throw error
		}
	}
}

export default DBManager
