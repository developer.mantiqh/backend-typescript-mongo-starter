import { Formatter, HttpCodes } from '@utils/response-formatter'

const auth: Controller = async function (_request, response) {
	try {
		return new Formatter(response, HttpCodes.SUCCESS, 'Success.').success()
	} catch (error) {
		return new Formatter(response, HttpCodes.INTERNAL_SERVER_ERROR, 'Internal server error.', error).error()
	}
}

export default auth
