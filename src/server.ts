import Express, { Application } from 'express'
import Morgan from 'morgan'

import Routes from '@routes/index'
import { Logger } from '@utils/logger'
import DB from '@db/index'

const app: Application = Express()

async function main() {
	try {
		await DB.connect()
		// Configurations.
		app.use(Express.json())
		app.use(Express.urlencoded({ extended: true }))
		app.use(Morgan('dev'))
		app.use('/api', Routes)
		app.listen(process.env.PORT, () => {
			Logger.info('Server started.', `http://localhost:${process.env.PORT}/api`)
		})
	} catch (error) {
		Logger.error('Failed to start server.', error)
		process.exit(1)
	}
}

main()
