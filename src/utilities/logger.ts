/* eslint-disable no-console */
import Moment from 'moment'

export class Logger {
	static info(...messages: unknown[]) {
		console.info(Moment().format('DD.MM.YYYY hh:mm:ss.sss ZZ'), 'INFO: ', ...messages)
	}

	static error(...messages: unknown[]) {
		console.info('ERROR: ', ...messages)
	}

	static debug(...messages: unknown[]) {
		console.info('DEBUG: ', ...messages)
	}
}
