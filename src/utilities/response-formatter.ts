export enum HttpCodes {
	SUCCESS = 200,
	CREATED = 201,
	ACCEPTED = 202,
	NO_CONTENT = 204,
	BAD_REQUEST = 400,
	UNAUTHORIZED = 401,
	FORBIDDEN = 403,
	NOT_FOUND = 404,
	INTERNAL_SERVER_ERROR = 500,
	NOT_IMPLEMENTED = 501
}

export class Formatter {
	private response: AppResponse
	private statusCode: HttpCodes
	private message: string
	private data?: Data

	constructor(response: AppResponse, statusCode: HttpCodes, message: string, data?: Data) {
		this.response = response
		this.statusCode = statusCode
		this.message = message
		this.data = data
	}

	public success(): AppResponse {
		return this.response.status(this.statusCode).json({
			statusCode: this.statusCode,
			message: this.message,
			data: this.data
		})
	}

	public error(): AppResponse {
		return this.response.status(this.statusCode).json({
			statusCode: this.statusCode,
			message: this.message,
			error: this.data
		})
	}
}
